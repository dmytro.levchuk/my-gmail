export const feedbackValidate = inputs => {
    console.log('=========', 'feedbackValidate')
    const errors = {};
    if (!inputs.email) {
        errors.email = 'Введите ваш email';
    }

    if (!inputs.msg || inputs.length < 3) {
        errors.msg = 'Вы забыли ввести сообщение';
    }
    return errors;
};

export const requiredInput = (input) => {
    console.log('=========', input);
    return input ? undefined : `Требуется ввод`;
}

export const correctInput = (input) => {
    return input.length < 3 ? 'Слишком короткий email' : undefined;
}

export const matchInput = (input, allInputs) => {
    return input === allInputs.email ? undefined :'Email\'ы не совпадают';
}