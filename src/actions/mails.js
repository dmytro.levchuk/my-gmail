export const DEL_EMAIL = 'DEL_EMAIL';
export const ADD_SENT_EMAIL = 'ADD_SENT_EMAIL';
export const ISREAD_EMAIL = 'ISREAD_EMAIL';
export const NEW_MESSAGE = 'NEW_MESSAGE';
export const COUNTER_UNREAD = 'COUNTER_UNREAD';
export const CHANGE_STAR = 'CHANGE_STAR';
export const SET_READ_MARKED_MESSAGES = 'SET_READ_MARKED_MESSAGES';
export const DEL_MARKED_MESSAGES = 'DEL_MARKED_MESSAGES';
export const RESET_MARKED = 'RESET_MARKED';
export const SET_MARKED = 'SET_MARKED';
export const CREATE_DRAFT = 'CREATE_DRAFT';



export function deleteCheckedMsg(markedID, mailList) {
    return dispatch => {
        let newMailList = {...mailList };
        for(let key in newMailList){
            newMailList[key] = newMailList[key].filter( (item) => {
                return markedID.includes(item.id) ? null : item
            })
        }
        dispatch({ type: DEL_MARKED_MESSAGES, payload: {mailList: newMailList} });
    }

}


export function markCheckedRead(markedID, mailList) {
    return dispatch => {
        let newMailList = {...mailList}
        for (let key in newMailList) {
            newMailList[key] = newMailList[key].filter((item) => {
                if (markedID.includes(item.id)) {
                    item.read = true;
                }

                return item
            })
        }
        dispatch({type: SET_READ_MARKED_MESSAGES, payload: {mailList: newMailList}});
        dispatch({type: COUNTER_UNREAD});
    }

}


export function changeStar(starID, mailList) {
    return dispatch => {
        let newMailList = {...mailList}
        for (let key in newMailList) {
            newMailList[key] = newMailList[key].map((item) => {
                if (item.id === starID) {
                    item.starred = !item.starred;
                }

                return item
            })
        }
        dispatch({type: CHANGE_STAR, payload: {mailList: newMailList}})
    }

}

export function countUnread() {
    return dispatch => {
        dispatch({type: COUNTER_UNREAD})
    }
}

export function set_isMarked() {
    return dispatch => {
        dispatch({type: COUNTER_UNREAD})
    }
}

export function reset_isMarked() {
    return dispatch => {
        dispatch({type: COUNTER_UNREAD})
    }
}

export function add_draft(letter) {
    return dispatch => {
        dispatch({type: CREATE_DRAFT, payload: {letter: letter}})
    }
}




