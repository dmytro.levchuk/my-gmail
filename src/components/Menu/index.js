import React, {Component, Fragment} from 'react';
import {NavLink} from 'react-router-dom'
import {connect} from 'react-redux'
import './menu.scss';
import {COUNTER_UNREAD, NEW_MESSAGE} from "../../actions/mails";

class Menu extends Component {

    state = {
        folders: [
            {
                id: 'getted',
                name: 'Inbox',
                icon: '/img/menu_inbox_unmarked.png'
            },
            {
                id: 'sent',
                name: 'Sent',
                icon: '/img/menu_sent.png'
            },
            {
                id: 'starred',
                name: 'Starred',
                icon: '/img/menu_star.png'
            },
            {
                id: 'draft',
                name: 'Draft',
                icon: '/img/menu_draft.png'
            }
        ]
    }

    componentWillMount() {
        this.props.countUnread();
    }

    render() {

        let folderList = this.state.folders.map((item) => {


            let folderURL = '/mails/' + item.id;
            let unreadMessages = '';
            if (item.id === 'getted' && this.props.count !== 0) {
                unreadMessages = <p className='unread'>{this.props.count}</p>
            }

            return <li key={item.id}>
                <NavLink to={folderURL} className='nav_item' activeClassName='active-menu'>
                    <div className='nav_item-left'>
                        <img className='nav_item_img' src={item.icon}/>
                        {item.name}
                    </div>
                    {unreadMessages}
                </NavLink>

            </li>;

        })


        return (
            <div>
                <input className='menu-compose' type="button" value='Compose'
                       onClick={() => {
                           this.props.openWindow()
                       }
                       }/>
                <ul className='nav-menu'>
                    {folderList}
                </ul>
            </div>
        );


    }

}


const mapStateToProps = (state) => {

    return {
        count: state.mails.counterUnread,
    }
}

const mapDispatchToProps = (dispatch) => {


    return {

        countUnread: () => {
            dispatch({type: COUNTER_UNREAD})
        },

        openWindow: () => {
            // let showCompose = stat;
            dispatch({type: NEW_MESSAGE})

        },

    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Menu);