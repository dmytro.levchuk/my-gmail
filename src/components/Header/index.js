import React, {Component, Fragment} from 'react';
import {NavLink} from 'react-router-dom'

import Search from './Search';
import UserControl from './UserControl';

import './header.scss'

class Header extends Component {

	render() {

		return (
			<header className='main-header'>
                <NavLink to="/">
                    <div className='header-logo'>
                        <img className='header-logo-img' src='https://ssl.gstatic.com/ui/v1/icons/mail/rfr/logo_gmail_lockup_default_1x.png' alt='Image is not awailable' />
                    </div>
				</NavLink>


				<div className='search-block'>
					<Search />
				</div>
				<div className='control'>
					<UserControl />
				</div>


                <NavLink to="/contacts">Contacts</NavLink>


			</header>
			
		);
	}

}

export default Header;

