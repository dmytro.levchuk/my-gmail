import React, {Component, Fragment} from 'react'
import {NavLink} from 'react-router-dom'

import './menucontacts.scss'

class MenuContacts extends Component {

    state = {
        menuList: [
            {
                id: 'contacts',
                name: 'Contacts'
            },
            {
                id: 'allEMails',
                name: 'Freguently contacted'
            }
        ]
    }

    render() {

        let menuList = this.state.menuList.map((elem) => {
            let link = '/contacts/' + elem.id;
            return <li key={elem.id}>
                <NavLink to={link} className='contacts-nav_item' activeClassName='contacts-active-menu'>
                    {elem.name}
                </NavLink>
            </li>
        });

        return (
            <ul>
                {menuList}
            </ul>
        )
    }
}

export default MenuContacts