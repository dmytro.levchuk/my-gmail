import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import {NavLink} from 'react-router-dom'

class ContactList extends Component {
    render() {

        console.log(this.props.contactsList);

        let listConts = this.props.contactsList.map((elem) => {
            let link = '/contacts/contacts/' + elem.id
            return <li key={elem.id}>
                <NavLink to={link}>
                    {elem.name} - {elem.email}
                </NavLink>
            </li>
        })

        return (
            <ul>
                {listConts}
            </ul>
        )
    }
}

const mapStateToProps = (state) => {
    return {
        contactsList: state.contacts.contactList,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {}
}

export default connect(mapStateToProps, mapDispatchToProps)(ContactList)