import React, {Component, Fragment} from 'react';
import {Route} from 'react-router-dom'


import HeaderContacts from './HeaderContacts'
import MenuContacts from './MenuContacts'
import ContactList from './ContactList'

class Contacts extends Component {

    render() {
        return (
            <Fragment>
                <HeaderContacts/>
                <div className='main-region'>
                    <Route path='/contacts' component={MenuContacts}/>

                    <Route exact path='/contacts' component={ContactList}/>
                    <Route exact path='/contacts/contacts' component={ContactList}/>

                </div>
            </Fragment>
        );
    }
}

export default Contacts;