import React, {Fragment, Component} from 'react'
import {NavLink} from 'react-router-dom'

import './headercontacts.scss'

class HeaderContacts extends Component {
    render () {
        return (
            <header className='header-contacts'>
                <NavLink to='/contacts' className='header-contacts-logo-link'>
                    <div className='header-contacts-logo'>
                        <img src="/img/logo_contacts.png" className='header-contacts-logo-img'/>
                        <p className='header-contacts-logo-text'>
                            Contacts
                        </p>
                    </div>

                </NavLink>

                <input className='header-search-input' type='text' value='' placeholder='Search...' />

                <NavLink to='/'>
                    Gmail
                </NavLink>

            </header>
        )
    }


}

export default HeaderContacts