import React, {Component, Fragment} from 'react';
import {NavLink} from 'react-router-dom'
import {connect} from 'react-redux'
import {
    deleteCheckedMsg,
    markCheckedRead,
    changeStar,
    countUnread,
    set_isMarked,
    reset_isMarked
} from '../../actions/mails'

import './mailList.scss'

class MailList extends Component {

    state = {
        checkedMsg: [],
        currentDir: ''
    }

    componentWillUpdate(nextProps, nextState) {

        console.log(nextProps.match.url);
        console.log(nextState.currentDir);

        if (!nextProps.match.url.includes(nextState.currentDir) && nextProps.match.params.folder) {
            this.props.reset_isMarked();
            this.setState({
                checkedMsg: [],
            })
        }
    }

    componentDidMount() {

        this.props.countUnread();

    }


    changeAllBox(arrMails, folder) {
        let arr = arrMails;
        let arrRes = [];
        if (this.state.checkedMsg.length > 0) {
            arrRes = [];
            this.props.reset_isMarked();
        } else {
            arr[folder].forEach((item) => {
                arrRes.push(item.id)
            });
            this.props.set_isMarked();
        }

        this.setState({
            checkedMsg: arrRes,
        })
    }

    changeBox(value) {

        let arr = this.state.checkedMsg;
        let index = this.state.checkedMsg.indexOf(value);
        if (index === -1) {
            arr.push(value);
            this.props.set_isMarked();
        } else {
            arr.splice(index, 1);

            if (arr.length === 0) {
                this.props.reset_isMarked();
            }

        }
        this.setState({
            checkedMsg: arr,
        })

    }


    render() {

        let folder = this.props.match.params.folder ? this.props.match.params.folder : 'getted';
        let trueFolder = this.props.match.params.folder;
        console.log('trueFolder = ' + trueFolder);
        this.state.currentDir = folder;
        let img_star = '';
        let img_checkbox = '';

        let mailList = [];

        if (folder === 'starred') {
            folder = 'getted';
        }

        mailList = this.props.mails[folder].map((item) => {

            if ((trueFolder === 'starred' && item.starred) || (['getted', 'sent', 'draft'].includes(trueFolder) || !trueFolder)) {

                (item.starred) ? img_star = '/img/star_yellow.png' : img_star = '/img/star_empty.png';

                (this.state.checkedMsg.includes(item.id)) ? img_checkbox = '/img/checkbox_plus.png' : img_checkbox = '/img/checkbox_empty.png';

                let mailURL = `/mails/${folder}/${item.id}`;
                return (<li key={item.id} className='mail-item'>
                    <div className='mail-item-chechbox'>
                        <img src={img_checkbox} alt="" className='mail-item-chechbox-img'
                             onClick={() => this.changeBox(item.id)}/>
                    </div>

                    <div className='mail-item-star'>
                        <img src={img_star} alt="" className='mail-item-star-img'
                             onClick={() => this.props.changeStar(item.id, this.props.mails)}/>
                    </div>

                    <NavLink to={mailURL} className={item.read ? 'letter' : 'letter active-letter'}>
                        <div className='letter-from'>
                            {item.from}
                        </div>
                        <div className='letter-subject'>
                            {item.subject}
                        </div>
                    </NavLink>

                    <div className='letter-navmenu'>
                        <div className='mail-item-set_asread'>
                            <img src='/img/set_asread.png' alt="" className='mail-item-star-img'
                                 onClick={() => this.props.markCheckedRead([item.id], this.props.mails)}/>
                        </div>
                        <div className='mail-item-delete'>
                            <img src='/img/delete_btn.png' alt="" className='mail-item-star-img'
                                 onClick={() => this.props.deleteCheckedMsg([item.id], this.props.mails)}/>
                        </div>
                    </div>
                </li>)

            }
        })


        let img_all_checkbox = '';

        if (this.state.checkedMsg.length > 0) {
            img_all_checkbox = '/img/checkbox_minus.png'
        } else {
            img_all_checkbox = '/img/checkbox_empty.png'
        }

        return (
            <div className='section-list'>
                <div className='top-list'>
                    <div className='mail-item-chechbox'>
                        <img src={img_all_checkbox} alt="" className='mail-item-chechbox-img'
                             onClick={() => this.changeAllBox(this.props.mails, folder)}/>
                    </div>

                    <div className='mail-item-set_asread'>
                        <img src='/img/set_asread.png' alt="" className='mail-item-chechbox-img'
                             onClick={() => this.props.markCheckedRead(this.state.checkedMsg, this.props.mails)}/>
                    </div>
                    <div className='mail-item-delete'>
                        <img src='/img/delete_btn.png' alt="" className='mail-item-chechbox-img'
                             onClick={() => this.props.deleteCheckedMsg(this.state.checkedMsg, this.props.mails)}/>
                    </div>


                </div>
                <div className='lists-content'>
                    <ul className='mail-list'>
                        {mailList}
                    </ul>
                </div>
            </div>


        )


    }

}

const mapStateToProps = (state) => {

    return {
        mails: state.mails.mailList,
        isMarked: state.mails.isMarked,
    }
}

const mapDispatchToProps = (dispatch) => {


    return {
        countUnread: () => dispatch(countUnread()),
        set_isMarked: () => dispatch(set_isMarked()),
        reset_isMarked: () => dispatch(reset_isMarked()),
        changeStar: (markedID, mailList) => dispatch(changeStar(markedID, mailList)),
        markCheckedRead: (markedID, mailList) => dispatch(markCheckedRead(markedID, mailList)),
        deleteCheckedMsg: (markedID, mailList) => dispatch(deleteCheckedMsg(markedID, mailList)),
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(MailList);