import React, { Component } from 'react';

const myTextarea = (props) => {
    const {input, type, placeholder, className} = props;
    return (
        <div>
            <textarea {...input} type={type} placeholder={placeholder} className={className} />
        </div>
    )
}

export default myTextarea;