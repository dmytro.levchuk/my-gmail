import React, {Component} from 'react'
import {connect} from 'react-redux'
import {SubmissionError} from 'redux-form';
import LetterForm from './LetterForm'

import './newLetter.scss'
import {ADD_SENT_EMAIL, changeStar, add_draft} from "../../actions/mails";

class NewLetter extends Component {

    state = {
        id: '',
        read: false,
        from: '',
        subject: '',
        text: ''
    }

    submit = values => {

        this.props.generateMail(values)

    };

    createDraft() {
        let newDraftLetter = {};

        for (let key in this.props.letter.values) {
            if (key === 'id') {
                newDraftLetter[key] = Math.random() * 100000;
            } else if (key === 'read') {
                newDraftLetter[key] = false;
            } else {
                if (!this.props.letter.values[key]) {
                    newDraftLetter[key] = 'default ' + key;
                } else {
                    newDraftLetter[key] = this.props.letter.values[key];
                }
            }
        }

        this.props.add_draft(newDraftLetter);
    }


    getInitialValues = () => {
        return {
            id: '',
            read: false,
            from: '',
            subject: '',
            text: ''
        }
    }

    render() {

        let classWindow = (this.props.status) ? 'new-letter' : 'new-letter unactive-new-letter';

        return (
            <div className={classWindow}>
                <div className='new-letter-header'>
                    <p>New letter</p>
                    <input className='new-letter-header-btn' type="button" value='X'
                           onClick={() => this.createDraft()}/>
                </div>
                <LetterForm onSubmit={this.submit} value={this.props.letter}  initialValues={this.getInitialValues()}/>

            </div>
        )
    }


    // initialState() {
    //
    //     this.props.generateMail(this.state)
    //
    //
    //     this.setState({
    //         id: '',
    //         read: false,
    //         from: '',
    //         subject: '',
    //         text: ''
    //     })
    // }


    // changeHandler = event => {
    //
    //     let name = event.target.name;
    //     let value = event.target.value;
    //     this.setState({
    //             [name]: value,
    //         }
    //     )
    // }

    // render() {
    //
    //     let classWindow = (this.props.status) ? 'new-letter' : 'new-letter unactive-new-letter';
    //
    //     return (
    //         <div className={classWindow}>
    //             <div className='new-letter-header'>
    //                 <p>New letter</p>
    //                 <input className='new-letter-header-btn' type="button" value='X'
    //                        onClick={() => this.initialState()}/>
    //             </div>
    //             <form action="">
    //                 <p>
    //                     <label>to</label><input type="email" value={this.state.from} placeholder='e-mail' name='from'
    //                                             onChange={this.changeHandler}/>
    //                 </p>
    //                 <p>
    //                     <label>subject</label><input type="text" value={this.state.subject} placeholder='subject'
    //                                                  name='subject' onChange={this.changeHandler}/>
    //                 </p>
    //                 <div>
    //                     <textarea className='new-letter-text' value={this.state.text} name='text'
    //                               onChange={this.changeHandler}></textarea>
    //                 </div>
    //                 <input className='new-letter-send-btn' type="button" value='send'
    //                        onClick={() => this.initialState()}/>
    //             </form>
    //
    //         </div>
    //     )
    // }
}

const mapStateToProps = (state) => {
    return {
        status: state.mails.showCompose,
        letter: state.form.newLetter,
    }
}

const mapDispatchToProps = (dispatch) => {
    return {

        add_draft: (mail) => dispatch(add_draft(mail)),

        generateMail: (data) => {

            let letter = {
                ...data,
                id: Math.random() * 100000,
                subject: data.subject || 'no subject'
            }
            dispatch({type: ADD_SENT_EMAIL, payload: {letter: letter}})

        },


    }
}


export default connect(mapStateToProps, mapDispatchToProps)(NewLetter)