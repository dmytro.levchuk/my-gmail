import React, { Fragment } from 'react';

const myInput = (props) => {
    const {input, type, placeholder} = props;
    // const errorClass = meta.error ? 'error' : null
    return (
        <Fragment>
            <input {...input} type={type} placeholder={placeholder}/>
        </Fragment>
    )
}
export default myInput;