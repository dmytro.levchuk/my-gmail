import React, {Component} from 'react';
import {Field, reduxForm} from 'redux-form';
import {feedbackValidate, requiredInput, correctInput, matchInput} from '../../validation'
import myInput from './myInput';
import myTextarea from './myTextarea';

import './newLetter.scss'

class LetterForm extends Component {

    state = {
        arrEmails: ['test', 'testtest', '111test', 'test11', 'mail', '222test111']
    }

    doSmth = (e) => {
        console.log('value in form - ' + e.target.value)
    }

    render() {
        console.log('===PROPS MAIL', this.props)
        const {handleSubmit, value} = this.props;
        let fromValue = '';

        for (let key in value) {
            console.log(key)
            if (key === 'values') {
                // console.log('------', value[key]);
                fromValue = value[key].from;
                console.log('from = ' + fromValue)
            }
        }

        return (

            <div>
                <form onSubmit={handleSubmit}>
                    <p>
                        <label>To</label><Field name="from" component={myInput}
                                                type="text" placeholder="Your e-mail..."
                                                onChange={this.doSmth}
                                                value={fromValue}
                    />
                    </p>

                    <p>
                        <label>Subject</label><Field name="subject" component={myInput}
                                                     type="text" placeholder="Your e-mail..."
                    />
                    </p>
                    <Field className='new-letter-text' name="text" component={myTextarea}
                           type="textarea" placeholder="Message"/>

                    <button className='new-letter-send-btn' type="submit" label="submit">Send</button>
                </form>

            </div>


        );
    }
}

export default reduxForm({form: 'newLetter'})(LetterForm);