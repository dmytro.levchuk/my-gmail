import React, {Component, Fragment} from 'react';
import {Switch, Route} from 'react-router-dom';

import {connect} from 'react-redux'
import {DEL_EMAIL} from '../../actions/mails'

import Header from '../Header';
import Menu from '../Menu';
import MailList from '../MailList';
import Mail from '../Mail';
import NewLetter from '../NewLetter/NewLetter';


class Dashboard extends Component {


    render() {


        return (
            <Fragment>
                <Header/>
                <div className='main-region'>
                    <Route  path="/" component={Menu} />

                    <Route exact path="/mails/:folder/:id" component={Mail} />
                    <Route exact path="/mails/:folder" component={MailList} />
                    <Route exact path="/" component={MailList} />

                </div>
                <div>
                    <NewLetter />
                </div>
            </Fragment>


        );
    }
}


export default Dashboard;