import React, {Component, Fragment} from 'react'
import {connect} from 'react-redux'
import {COUNTER_UNREAD, ISREAD_EMAIL} from "../../actions/mails";

class Mail extends Component {

    componentDidMount() {
        this.props.chgMailStatus(this.props.match.params.id, this.props.match.params.folder);
        this.props.countUnread();
    }

    render() {
        let mailInfo = this.props.mails[this.props.match.params.folder].filter((item) => {
            return item.id === Number(this.props.match.params.id) ? item : null
        })

        let mailRender = 'Error...'
        if (mailInfo.length > 0) {
            mailRender = (
                <Fragment>
                    <h2>{mailInfo[0].subject}</h2>
                    <p>{mailInfo[0].text}</p>
                </Fragment>
            )
        }

        return (
            <Fragment>
                <div>
                    {mailRender}
                </div>
            </Fragment>
        );
    }
}

const mapStateToProps = (state) => {

    return {
        mails: state.mails.mailList,
    }
}

const mapDispatchToProps = (dispatch) => {


    return {

        chgMailStatus: (mailID, folder) => {

            dispatch({type: ISREAD_EMAIL, payload: {mailID: mailID, folder: folder}})

        },

        countUnread: () => {
            dispatch({type: COUNTER_UNREAD})
        }


    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Mail);