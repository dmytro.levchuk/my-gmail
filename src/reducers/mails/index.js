import {DEL_EMAIL, ADD_SENT_EMAIL, ISREAD_EMAIL, NEW_MESSAGE,
    COUNTER_UNREAD, CHANGE_STAR, SET_READ_MARKED_MESSAGES, DEL_MARKED_MESSAGES,
    RESET_MARKED, SET_MARKED, CREATE_DRAFT} from '../../actions/mails'


const initialState = {
    active: 'getted',
    showCompose: false,
    isMarked: false,
    counterUnread: 0,
    mailList: {
        getted: [
            {
                id: 1,
                read: false,
                marked: false,
                starred: false,
                from: 'test@test.us',
                subject: 'Some test subject',
                text: 'Вже давно відомо, що читабельний зміст буде заважати зосередитись людині, яка оцінює композицію сторінки. Сенс використання Lorem Ipsum полягає в тому, що цей текст має більш-менш нормальне розподілення літер на відміну від, наприклад, "Тут іде текст. Тут іде текст."'
            },
            {
                id: 2,
                read: false,
                marked: false,
                starred: false,
                from: 'test2@test.us',
                subject: 'Some test subject 2',
                text: 'Існує багато варіацій уривків з Lorem Ipsum, але більшість з них зазнала певних змін на кшталт жартівливих вставок або змішування слів, які навіть не виглядають правдоподібно.Існує багато варіацій уривків з Lorem Ipsum, але більшість з них зазнала певних змін на кшталт жартівливих вставок або змішування слів, які навіть не виглядають правдоподібно.'
            },
            {
                id: 3,
                read: false,
                marked: false,
                starred: false,
                from: 'test3@test.us',
                subject: 'Some test subject 3',
                text: 'Якщо ви збираєтесь використовувати Lorem Ipsum, ви маєте упевнитись в тому, що всередині тексту не приховано нічого, що могло б викликати у читача конфуз.'
            }
        ],
        sent: [
            {
                id: 4,
                read: false,
                marked: false,
                starred: false,
                from: 'test4@test.us',
                subject: 'Some test subject 4',
                text: 'Якщо ви збираєтесь використовувати Lorem Ipsum, ви маєте упевнитись в тому, що всередині тексту не приховано нічого, що могло б викликати у читача конфуз.'
            },
            {
                id: 5,
                read: false,
                marked: false,
                starred: false,
                from: 'test5@test.us',
                subject: 'Some test subject 5',
                text: 'Більшість відомих генераторів Lorem Ipsum в Мережі генерують текст шляхом повторення наперед заданих послідовностей Lorem Ipsum. '
            },
            {
                id: 6,
                read: false,
                marked: false,
                starred: false,
                from: 'test6@test.us',
                subject: 'Some test subject 6',
                text: 'Вже давно відомо, що читабельний зміст буде заважати зосередитись людині.'
            },
            {
                id: 7,
                read: false,
                marked: false,
                starred: false,
                from: 'test7@test.us',
                subject: 'Some test subject 7',
                text: 'Тут іде текст." Це робить текст схожим на оповідний. Багато програм верстування та веб-дизайну використовують.'
            }
        ],
        starred: [],
        draft: []
    }
}

function mails(state = initialState, action) {
    switch (action.type) {
        case COUNTER_UNREAD:
            let count = 0;
            state.mailList['getted'].forEach((elem) => {
                if (!elem.read) ++count;
            });
            return {...state, counterUnread: count}

        case RESET_MARKED:
            return {...state, isMarked: false}
        case SET_MARKED:
            return {...state, isMarked: true}

        case DEL_EMAIL:
            return {...state, ...action.payload}

        case SET_READ_MARKED_MESSAGES:
            return {...state, ...action.payload}

        case DEL_MARKED_MESSAGES:
            return {...state, ...action.payload}
        case CHANGE_STAR:
            return {...state, ...action.payload}

        case CREATE_DRAFT:
            let draftList = state.mailList['draft'];
            draftList.push(action.payload.letter);
            return {
                ...state,
                showCompose: false,
                mailList: {
                    getted: [...state.mailList.getted],
                    sent: [...state.mailList.sent],
                    draft: draftList
                },
            }


        case ADD_SENT_EMAIL:
            let mailListUpd = state.mailList['sent'];
            mailListUpd.push(action.payload.letter);
            return {
                ...state,
                showCompose: false,
                mailList: {
                    getted: [...state.mailList.getted],
                    draft: [...state.mailList.draft],
                    sent: mailListUpd
                },
            }
        case ISREAD_EMAIL:
            let folderEmail = [...state.mailList[action.payload.folder]]

            let updatedEmail = folderEmail.map( (item) => {
                let tmpItem = {...item}
                if(Number(action.payload.mailID) === Number(item.id)){
                    tmpItem.read = true
                }
                return tmpItem
            })

            return {
                ...state,
                mailList: {
                    getted: [...state.mailList.getted],
                    sent: [...state.mailList.sent],
                    draft: [...state.mailList.draft],
                    [action.payload.folder]: [...updatedEmail]
                }
            }

        case NEW_MESSAGE:

            return {...state, showCompose: !state.showCompose};
        default:
            return {...state}
    }
}

export default mails