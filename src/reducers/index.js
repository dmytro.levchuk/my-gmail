import {combineReducers} from 'redux';
import {reducer as formReducer} from 'redux-form';
import mails from './mails';
import contacts from './contacts';

import {ADD_SENT_EMAIL} from "../actions/mails";

const rootReducer = combineReducers({
    mails,
    contacts,
    form: formReducer.plugin({
        newLetter: (state, action) => {
            switch (action.type) {
                case ADD_SENT_EMAIL:
                    return undefined; // <--- blow away form data
                default:
                    return state;
            }
        }
    }),

});

export default rootReducer;