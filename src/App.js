import React, { Component, Fragment } from 'react';
import { Switch, Route } from 'react-router-dom';

// import HeaderContacts from './components/HeaderContacts';
// import Menu from './components/Menu';
// import MailList from './components/MailList';
// import NewLetter from './components/NewLetter/NewLetter';

import './App.scss';
import Contacts from "./components/Contacts";
import Dashboard from "./components/Dashboard";

class App extends Component {

    render() {
        return(
            <Fragment>
                <Switch>
                    <Route exact path="/" component={Dashboard} />
                    <Route path="/mails" component={Dashboard} />
                    <Route path="/contacts" component={Contacts} />
                </Switch>
            </Fragment>
        )
    }


}

export default App;